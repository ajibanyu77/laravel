<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Halaman Form</title>
</head>

<body>
    <h1>Buat Account Baru!</h1>
    <h2>Sign Up Form</h2>
    <form action="{{ url('/welcome') }}">
        <label for="first_name">First Name :</label>
        <br><br>
        <input type="text" id="first_name" name="first_name">
        <br><br>
        <label for="last_name">Last Name :</label>
        <br><br>
        <input type="text" id="last_name" name="last_name">
        <br><br>
        <label for="gender">Gender :</label>
        <br><br>
        <input type="radio" id="male" name="gender" value="male">
        <label for="male">Male</label><br>
        <input type="radio" id="female" name="gender" value="female">
        <label for="female">Female</label><br>
        <input type="radio" id="other" name="gender" value="other">
        <label for="other">Other</label><br><br>
        <label for="nationality">Nationality :</label>
        <br><br>
        <select name="nationality" id="nationality">
            <option value="Indonesia">Indonesia</option>
            <option value="Singapore">Singapore</option>
            <option value="Malaysia">Malaysia</option>
            <option value="Australia">Australia</option>
        </select>
        <br><br>
        <label for="language">Language Spoken :</label>
        <br><br>
        <input type="checkbox" id="indonesia" name="indonesia" value="indonesia">
        <label for="indonesia">Bahasa Indonesia</label><br>
        <input type="checkbox" id="English" name="English value=" English">
        <label for="English">English</label><br>
        <input type="checkbox" id="other" name="other" value="other">
        <label for="other">Other</label><br><br>
        <label for="bio">Bio :</label>
        <br><br>
        <textarea name="bio" id="bio" cols="30" rows="10"></textarea>
        <br><br>
        <input type="submit" value="submit">

    </form>
</body>

</html>